//
//  AddPaymentView.swift
//  cards
//
//  Created by Alex on 07.05.2020.
//  Copyright © 2020 Alex. All rights reserved.
//


import SwiftUI
import CoreData
struct AddPaymentView: View {
    
     @Environment(\.presentationMode) var presentationMode
      @Environment(\.managedObjectContext) var moc
    @State private var paymentAmount  = ""
    @State private var pickerPaymentDate  = Date()
    @State  var currentCreditCardOnScreen: CreditCard
   
   
    
    
       
    
    
    
    var body: some View {
       NavigationView{
                 Form{
                     Section{
                
                     
                        TextField("Сумма досрочного платежа", text: $paymentAmount)
                        .keyboardType(.decimalPad)
                     
                  
                  }
                     Section{
                         Button("Save"){
       
                        
                            let newPayment = PaymentItem(context: self.moc)
                            newPayment.summ_for_pay = Double(self.paymentAmount)!
                            
                            newPayment.parentCategory = self.currentCreditCardOnScreen
                          
                   
                                         
                                              try? self.moc.save()
                                              self.presentationMode.wrappedValue.dismiss()
                        
                            self.presentationMode.wrappedValue.dismiss()
                         }
                     }
                 }.navigationBarTitle("Досрочный платеж")
         .environment(\.locale, Locale(identifier: "ru"))
             }
    }
}

