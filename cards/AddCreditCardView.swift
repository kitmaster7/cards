//
//  AddCreditCardView.swift
//  cards
//
//  Created by Alex on 07.05.2020.
//  Copyright © 2020 Alex. All rights reserved.
//

import Foundation


import SwiftUI
import CoreData
struct AddCreditCardView: View {
    @Binding var pages : [PageViewData]
    @Environment(\.managedObjectContext) var moc
     @Environment(\.presentationMode) var presentationMode
    @State  var name = ""
 
    var  addCreditCardViewModel = AddCreditCardViewModel()
   
    
    var body: some View {
        VStack{
                    TextField("Credit name", text: $name)
                 
                
                 
                        Button("Save"){
                         
                            let creditCard = CreditCard(context:self.moc)
                            creditCard.name = self.name
                            creditCard.createdAt = Date()
                          
                            
                            
                            
                            
                            for payment in  self.addCreditCardViewModel.calculateSomething(){
                                
                                let newPayment = PaymentItem(context: self.moc)
                                newPayment.summ_for_pay = Double(payment.amount)!
                                                        
                                                        newPayment.parentCategory = creditCard
                                                      
                                             
                                                                      
                                                                     
                                                                   
                            }
                            
                            
                            
                          
                            
                                   try? self.moc.save()
                            
                              self.pages.append(PageViewData(creditCard: creditCard))
                            self.presentationMode.wrappedValue.dismiss()
                        }
                 
        }
      
    }
}

