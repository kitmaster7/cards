//
//  SwiperView.swift
//  cards
//
//  Created by Alex on 07.05.2020.
//  Copyright © 2020 Alex. All rights reserved.
//

import SwiftUI

struct SwiperView: View {
    let pages: [PageViewData]
    
    @Binding var index: Int
    @State private var offset: CGFloat = 0
    @State private var isUserSwiping: Bool = false
       let spacing: CGFloat = 10
    var body: some View {
        GeometryReader { geometry in
            ScrollView(.horizontal, showsIndicators: true) {
               HStack(spacing: self.spacing) {
                    ForEach(self.pages) { viewData in
                        PageView(viewData: viewData)
                            .frame(width: geometry.size.width,
                                   height: geometry.size.height)
                    }
                }
            }
            .content
            .offset(x:self.offset )
            .frame(width: geometry.size.width, alignment: .leading)
                
            .gesture(
                DragGesture()
                    .onChanged({ value in
                        self.isUserSwiping = true
                      
                        
                             self.offset = value.translation.width - geometry.size.width * CGFloat(self.index)
                        
                        
                        
                        print(self.offset)
                    })
                    .onEnded({ value in
                        if value.translation.width < geometry.size.width / 2, self.index < self.pages.count - 1 {
                            self.index += 1
                        }
                        if value.translation.width > geometry.size.width / 2, self.index > 0 {
                            self.index -= 1
                        }
                        withAnimation {
                      
                                                         self.offset = -(geometry.size.width + self.spacing) * CGFloat(self.index)
                                                                                       
                            self.isUserSwiping = false
                        }
                    })
            )
        }
 
                                              .background(Color.pink)
                                                                                 .foregroundColor(.white)
                                                                               .frame(height: 100)
                                                                               .padding(20)
    }
}
