//
//  PageView.swift
//  cards
//
//  Created by Alex on 07.05.2020.
//  Copyright © 2020 Alex. All rights reserved.
//

import Foundation
import SwiftUI

struct PageViewData: Identifiable {
    let id: String = UUID().uuidString
    let creditCard: CreditCard
}



struct PageView:  View {
    
    let viewData: PageViewData
    var body: some View {
        RoundedRectangle(cornerRadius: 15)
            .fill(Color.blue)
            
            .overlay(
                VStack{
                    VStack(alignment: .leading){
                        Text(viewData.creditCard.name ?? "Unkonw title")
                            .font(.headline)
                        
                    }
                    
                }
        )
    }
    
}



