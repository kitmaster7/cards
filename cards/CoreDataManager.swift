//
//  CoreDataManager.swift
//  cards
//
//  Created by Alex on 07.05.2020.
//  Copyright © 2020 Alex. All rights reserved.
//


import Foundation
import CoreData
import UIKit
extension NSManagedObjectContext{
    static var current: NSManagedObjectContext{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
}

class CoreDataManager{
    
    
    //static let shared = CoreDataManager(moc: NSManagedObjectContext.current)
    
    
    
    
    
    var moc: NSManagedObjectContext
    
    init(moc:NSManagedObjectContext){
        self.moc = moc
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let persistentContainer =  appDelegate.persistentContainer
        
        
        return persistentContainer
    }()
    
    
    func getAllCreditItemsForCreditCard(creditCardName:String) -> [PaymentItem] {
        let startTime = CFAbsoluteTimeGetCurrent()
          print(" TimeSTART: \(startTime)")
        let creditCardPredicate = NSPredicate(format: "parentCategory.name == %@",creditCardName)
        let request: NSFetchRequest<PaymentItem> = PaymentItem.fetchRequest()
        
        request.predicate = creditCardPredicate
        var paymentItems = [PaymentItem]()
        let taskContext = persistentContainer.newBackgroundContext()
        print("START LOADING1111")
        taskContext.performAndWait {
            
            do {
                paymentItems =  try self.moc.fetch(request)
            }
                
                
            catch let error as NSError {
                print(error)
            }
            
        }
            print("END LOADING")
        
        
          print(" TimeEND: \(CFAbsoluteTimeGetCurrent())")
        let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
               print(" Time: \(timeElapsed)")
        
        
        
        
        return paymentItems
        
    }
    
    
    func saveCoreData() {
        if self.moc.hasChanges{
            
            do {
                try self.moc.save()
            } catch let error as NSError {
                print(error)
            }
        }
    }
    
}

