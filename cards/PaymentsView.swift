//
//  PaymentsView.swift
//  cards
//
//  Created by Alex on 07.05.2020.
//  Copyright © 2020 Alex. All rights reserved.
//

import SwiftUI
import CoreData
struct PaymentsView: View {
    
    @Environment(\.managedObjectContext) var moc

    var payments: [PaymentItem]
  //  @FetchRequest var payments: FetchedResults<PaymentItem>
    
    var creditCardName:String = ""
    
    
   
     init(creditCardName:String){
        print("INIT \(creditCardName)")
        self.creditCardName = creditCardName
        payments = []
        
       
    
    }
    func getCreditItemsFromCoreData(request: NSFetchRequest<PaymentItem>) -> [PaymentItem] {
       print("GET ITEMS")
        let start = CFAbsoluteTimeGetCurrent()
                 
        
        
        var payments = [PaymentItem]()
            do {
                payments =  try self.moc.fetch(request)
            } catch let error as NSError {
                print(error)
            }
        
        
        let diff = CFAbsoluteTimeGetCurrent() - start
                         print("Took \(diff) seconds FOR \(creditCardName)")
            return payments
        }
    
    func getData() -> [PaymentItem]{
       print("GET DATAAA")
               //print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))
              
        
          let request: NSFetchRequest<PaymentItem> = PaymentItem.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(keyPath: \PaymentItem.summ_for_pay, ascending: true)]
       let creditCardPredicate =           NSPredicate(format: "parentCategory.name == %@",creditCardName)
               
               request.predicate = creditCardPredicate
               
        
            return  getCreditItemsFromCoreData(request: request)
       
        
      
          
    }

       
        
      
    
    var body: some View {
        VStack{
          
        List{
           
                           
                ForEach(self.getData(), id: \.self) { payment in
                               
                               VStack(alignment: .leading){
                                   Text("\(payment.summ_for_pay)")
                                       .font(.headline)
                                   
                               }
                           }
                           
                       }
       
        }
    }
}

struct PaymentsView_Previews: PreviewProvider {
    static var previews: some View {
        PaymentsView(creditCardName: "alex")
    }
}
