//
//  ContentView.swift
//  cards
//
//  Created by Alex on 07.05.2020.
//  Copyright © 2020 Alex. All rights reserved.
//

import SwiftUI
import CoreData


enum ActiveSheet {
    case addNewCreditCardSheet, addNewPaymentSheet
}

struct ContentView: View {
    @State private var activeSheet: ActiveSheet = .addNewCreditCardSheet
    @State private var presentingSheet = false
    @State private var currentCreditCardOnScreen: CreditCard?
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(entity: CreditCard.entity(),
                  sortDescriptors: [ NSSortDescriptor(keyPath: \CreditCard.createdAt, ascending: false) ]
    ) var creditCards: FetchedResults<CreditCard>
    @State private var index = 0
    let spacing: CGFloat = 10
    @State private var offset: CGFloat = 0
    @State private var showingAddScreen = false
    
    
    
    @State var pages: [PageViewData] = [
        
    ]
    
    
    var body: some View {
        NavigationView{
            VStack{
                
                
                
                SwiperView(pages: self.pages, index: self.$index)
                HStack() {
                    ForEach(0..<self.pages.count, id:\.self) { index in
                        CircleButton(isSelected: Binding<Bool>(get: { self.index == index }, set: { _ in })) {
                            withAnimation {
                                self.index = index
                            }
                        }
                    }
                }
                
                
                
                
                
                
                
                
                
                
                
                GeometryReader { geometry in
                    ScrollView(.horizontal, showsIndicators: true) {
                        HStack(spacing: self.spacing) {
                            ForEach(self.creditCards, id: \.self){ creditCard in
                                GeometryReader { gr in
                                    
                                    
                                    
                                    RoundedRectangle(cornerRadius: 15)
                                        .fill(Color.red)
                                        
                                        .overlay(
                                            VStack{
                                                VStack(alignment: .leading){
                                                    Text(creditCard.name ?? "Unkonw title")
                                                        .font(.headline)
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                    )
                                    
                                }
                                .frame(width: geometry.size.width,height: 130)
                            }
                        }
                    }
                        
                    .content.offset(x: self.offset)
                    .frame(width: geometry.size.width, alignment: .leading)
                    .gesture( DragGesture()
                    .onChanged({ value in
                        
                        self.offset = value.translation.width - geometry.size.width * CGFloat(self.index)
                        print("GOOD \(self.offset)")
                        
                    })
                        .onEnded({ value in
                            
                            
                            
                            
                            if -value.translation.width > geometry.size.width / 2, self.index < self.creditCards.count - 1 {
                                self.index += 1
                                self.currentCreditCardOnScreen = self.creditCards[self.index]
                                print("INDEX IS CHANGED!!!!")
                            }
                            if value.translation.width > geometry.size.width / 2, self.index > 0 {
                                self.index -= 1
                                
                                self.currentCreditCardOnScreen = self.creditCards[self.index]
                            }
                            print("INDEX BEFORE ANIMATION \(self.index)")
                            
                            
                            withAnimation {
                                print("WITH ANIMATION")
                                print(self.index)
                                print(self.spacing)
                                
                                self.offset = -(geometry.size.width + self.spacing) * CGFloat(self.index)
                                
                                
                                
                            }
                        })
                    )
                    
                    
                }
                .background(Color.pink)
                .foregroundColor(.white)
                .frame(height: 100)
                .padding(20)
                //   Text(self.creditCardListVM.creditCards[index].name)
                Button(action: {
                    self.presentingSheet = true
                    self.activeSheet = .addNewPaymentSheet
                }, label: {
                    
                    Image(systemName:"plus.circle").font(.title)
                        .foregroundColor(.red)
                    
                    
                })
                
                if(self.creditCards.count > 0 ) {
                    PaymentsView(creditCardName: (self.creditCards[self.index].name!) ).environment(\.managedObjectContext,self.moc)
                }
                
            }.onAppear(perform: {
                
                for oneCredit in self.creditCards{
                    
                    self.pages.append(PageViewData(creditCard: oneCredit))
                }
                
                
                
                
                
                
            })
                .navigationBarTitle(Text("Cards"), displayMode: .inline)
                
                .navigationBarItems(trailing:
                    Button(action:{
                        self.presentingSheet = true
                        self.activeSheet = .addNewCreditCardSheet
                    }){
                        Image(systemName:"plus")
                })
                .sheet(isPresented: $presentingSheet, onDismiss:{
                    if self.activeSheet == .addNewPaymentSheet {
                        
                    }
                }) {
                    
                    if self.activeSheet == .addNewCreditCardSheet {
                        AddCreditCardView(pages: self.$pages).environment(\.managedObjectContext,self.moc)
                        
                    } else {
                        AddPaymentView(currentCreditCardOnScreen: self.currentCreditCardOnScreen ?? self.creditCards[0]).environment(\.managedObjectContext,self.moc)
                    }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
